class FoxCape {
  public body: HTMLCollectionOf<HTMLElementTagNameMap['body']>;

  constructor() {
    this.body = document.getElementsByTagName( 'body' );

    this.initializeBody();
  }

  protected initializeBody() {
    this.body[0].classList.add('fox-cape--loaded');
  }

}

document.addEventListener('DOMContentLoaded', () => {
  if (-1 !== [ 'complete', 'interactive' ].indexOf(document.readyState)) {
    new FoxCape();
  }
}, false);
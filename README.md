# Fox Cape

## Index
1. [Prerequisites](#prerequisites)
1. [Getting started](#getting-started)

## Prerequisites
- NodeJS

## Getting started
First, use `npm install` to install all the dependencies project requires.

Project comes with `postinstall` script that will build all CSS and JS files used by project.
'use strict';

const { series, parallel, watch, task, src, dest } = require('gulp');

const sass = require('gulp-sass');
const ts = require('gulp-typescript');

const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');

const cleanCss = require('gulp-clean-css');
const terser = require('gulp-terser');

const entry = {
  sass: {
    watch  : './src/sass/**/*',
    source : './src/sass/fox-cape.scss',
    build  : './built/css/',
  },

  css: {
    source : './built/css/fox-cape.css',
    build  : './dist',
  },

  ts: {
    source : './src/ts/**/*',
    build  : './built/js',
  },

  js: {
    source : './built/js/fox-cape.js',
    build  : './dist/',
  }
};

/**
 * Stylesheets
 */
function compileSass() {
  return src(entry.sass.source)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(dest(entry.sass.build));
}

function copyCss() {
  return src(entry.css.source).pipe(dest(entry.css.build));
}

function minifyCss() {
  return src(entry.css.source)
    .pipe(rename('fox-cape.min.css'))
    .pipe(cleanCss())
    .pipe(dest(entry.css.build));
}

task('css:compile', () => {
  return compileSass();
});

task('css:copy', () => {
  return copyCss();
});

task('css:minify', () => {
  return minifyCss();
});

task('css:build', series( 'css:compile', 'css:copy', 'css:minify' ));

task('css:watch', () => {
  watch(entry.sass.watch, { usePolling: true }, series('css:build'));
});

/**
 * TypeScript
 */
function compileTypeScript() {
  return src(entry.ts.source)
    .pipe(sourcemaps.init())
    .pipe(ts({
      noImplicitAny : true,
      outFile       : 'fox-cape.js',
    }))
    .pipe(sourcemaps.write())
    .pipe(dest(entry.ts.build));
}

task('ts:compile', () => {
  return compileTypeScript();
});

task('ts:watch', () => {
  watch(entry.ts.source, series('ts:compile'));
});

/**
 * JavaScript
 */
function copyCompiledJavaScript() {
  return src(entry.js.source).pipe(dest(entry.js.build));
}

function uglifyJavaScript() {
  return src(entry.js.source)
    .pipe(rename('fox-cape.min.js'))
    .pipe(terser())
    .pipe(dest(entry.js.build));
}

task('js:copy', () => {
  return copyCompiledJavaScript();
});

task('js:uglify', () => {
  return uglifyJavaScript();
});

task('js:build', series( 'js:copy', 'js:uglify' ));

task('js:watch', () => {
  watch(entry.js.source, series('js:build'));
});

/**
 * Global
 */
task('build', series( 'css:build', 'ts:compile', 'js:build' ));
task('watch', parallel( 'css:watch', 'ts:watch', 'js:watch' ));

exports.default = series('build');